package com.curso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VentaappBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(VentaappBackendApplication.class, args);
	}
}
