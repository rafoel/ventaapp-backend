package com.curso.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.curso.model.Persona;

public interface IPersonaDAO extends JpaRepository<Persona, Integer>{

}
